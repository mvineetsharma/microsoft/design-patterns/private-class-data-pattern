﻿using System;

// Define an interface for the public contract
public interface IFruit
{
    void Display();
}

// Private implementation class
internal class FruitImpl : IFruit
{
    private string name;
    private string color;

    public FruitImpl(string name, string color)
    {
        this.name = name;
        this.color = color;
    }

    public void Display()
    {
        Console.WriteLine($"This is a {color} {name}.");
    }
}

// Public Fruit class that delegates implementation to FruitImpl
public class Fruit : IFruit
{
    private FruitImpl implementation;

    public Fruit(string name, string color)
    {
        implementation = new FruitImpl(name, color);
    }

    public void Display()
    {
        implementation.Display();
    }
}

class Program
{
    static void Main()
    {
        IFruit apple = new Fruit("Apple", "Red");
        IFruit banana = new Fruit("Banana", "Yellow");

        apple.Display();  // This is a Red Apple.
        banana.Display(); // This is a Yellow Banana.
    }
}
